<?php

require_once("QueryBuilder.php");

\QueryBuilder\Engine\DBConfig::$DATABASE = "test";
\QueryBuilder\Engine\DBConfig::$HOST = "localhost";
\QueryBuilder\Engine\DBConfig::$LOGIN = "root";
\QueryBuilder\Engine\DBConfig::$PASSWORD = "";
\QueryBuilder\Engine\DBConfig::runPDO();

$factory = new QueryBuilder();
$select = $factory->select();
$select->table()
            ->set('dhdhd')
            ->set('sdfsdf');
$select->select()
            ->column('id', 'tferef')
            ->column('sdfsdf', 'sdff');
$select->where()
            ->column('sdfsdf', 'sdfsdf')->is(3)->and()
            ->column('sdasd', 'asdasd')->isColumn('cdsdcs', 'sdfsdf')->and()
            ->combine()
                ->column('wtf')->is(5)->or()
                ->column('sdf')->is(5)
            ->endCombine();

$select->dependency()->join()->left()->table()->set('hjhh')->where()->column('id')->is('5');
$select->dependency()->join()->inner()->table()->set('hjhh')->where()->column('id')->is('5');

echo $select->render();


$db = new \QueryBuilder\Engine\Database();

echo $db->query($select);

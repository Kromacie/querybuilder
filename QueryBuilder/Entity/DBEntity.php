<?php
namespace QueryBuilder\Entity;

use QueryBuilder\Engine\Database;
use QueryBuilder\Query\Select;

abstract class DBEntity extends DBHandler
{

    protected $db;

    function __construct($table)
    {
        parent::__construct($table);
    }
    function get()
    {
        $db = new Database();
        $result = $db->query($this->getStatement());
        if(!$result)
        {
            return;
        }
        if($this->getStatement() instanceof Select)
        {
            while ($m = $result->fetch())
            {
                foreach ($m as $key => $value)
                {
                    if(property_exists($this, $key))
                    {
                        $this->$key = $value;
                    }
                }
            }

        }

    }

    function __toString()
    {
        $arr = [];
        foreach ($this as $key => $value)
        {
            $arr[$key] = $value;
        }
        return json_encode($arr);
    }

    /**
     * @return \PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param mixed $db
     */
    public function setDb(\PDO $db)
    {
        $this->db = $db;
    }
}
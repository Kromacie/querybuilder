<?php
namespace QueryBuilder\Engine;


use QueryBuilder\Query\Statement;

class Transaction
{
    private $db;
    protected $statements = [];

    /**
     * Transaction constructor.
     * @param \PDO $db
     */
    function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    function add(Statement $statement)
    {
        $this->statements[] = $statement;
        return $this;
    }


    function commit()
    {
        $this->db->beginTransaction();
        foreach ($this->statements as $statement)
        {
            $this->db->exec($statement->render());
        }
        $this->db->commit();
    }

    /**
     * @return \PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param \PDO $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }
}
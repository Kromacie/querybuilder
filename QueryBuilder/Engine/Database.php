<?php
namespace QueryBuilder\Engine;


use QueryBuilder\Query\Statement;

class Database
{
    private $db;
    protected $statements = [];

    /**
     * Translation constructor.
     * @param \PDO $db
     */
    function __construct(\PDO $db)
    {
        $this->db = $db;
    }
    function query(Statement $statement)
    {
        $query = $statement->render();
        $result =  $this->db->query($query);
        return $result;
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param mixed $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

}
<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Query\Modules\Where\Condition;
use QueryBuilder\Query\Statement;

/**
 *
 */
abstract class Where extends Handler implements ColumnAccess, Renderable
{
  protected $select;
  protected $rows = [];
  protected $combine;
  protected $lastRow;
  protected $buffer;
  protected $type;

  function __construct(Statement $select)
  {
    $this->buffer = new Condition($this);
    $this->select = $select;
  }
  function column($column, $table = false)
  {
    $condition = clone $this->buffer;
    $condition->setColumn($column);
    if($table)
    {
      $condition->setTable($table);
    }
    $this->rows[$column] = $condition;
    $this->buffer->clear();
    $this->lastRow = $condition;

    return $condition;
  }
  function combine()
  {
    $this->buffer->openBracket();
    return $this;
  }
  function endCombine()
  {
    $this->lastRow->closeBracket();
    return $this;
  }
  function render()
  {
    $query = "";
    foreach ($this->rows as $row)
    {
      $query .= " ".$row->render();
    }
    if($query != "")
    {
      return " $this->type".$query;
    }
    return "";

  }
}



 ?>

<?php
namespace QueryBuilder\Query\Modules\Where;


/**
 *
 */
class Logic
{
  protected $con;
  function __construct(Condition $con)
  {
    $this->con = $con;
  }
  function and()
  {
    $this->con->setLogic("AND");
    return $this->con->where();
  }
  function or()
  {
    $this->con->setLogic("OR");
    return $this->con->where();
  }
  function endCombine()
  {
    $this->con->where()->endCombine();
    return $this;
  }
}

 ?>

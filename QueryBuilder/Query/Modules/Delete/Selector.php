<?php
namespace QueryBuilder\Query\Modules\Delete;

use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Delete;

/**
 *
 */
class Selector implements Renderable
{
  protected $select;
  protected $operators = [];
  function __construct(Delete $select)
  {
    $this->select = $select;
  }
  function render()
  {
    return "DELETE ";
  }
}

 ?>

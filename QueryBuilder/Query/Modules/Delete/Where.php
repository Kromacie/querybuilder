<?php
namespace QueryBuilder\Query\Modules\Delete;

use QueryBuilder\Query\Statement;

class Where extends \QueryBuilder\Query\Modules\Where
{
    function __construct(Statement $select)
    {
        parent::__construct($select);
        $this->type = "WHERE";
    }
}
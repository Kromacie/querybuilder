<?php
namespace QueryBuilder\Query\Modules\Update\Selector;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\VariablesAccess;
use QueryBuilder\Query\Modules\Update\Selector;

/**
 *
 */
class Value implements Renderable, ColumnAccess, VariablesAccess
{
  protected $row;
  protected $value;
  protected $selector;

  function __construct(Selector $selector)
  {
    $this->selector = $selector;
  }
  function column($column)
  {
    $this->row = $column;
    return $this;
  }
  function set($value)
  {
    $this->value = $value;
    return $this->selector;
  }
  function render()
  {
    return $this->row."='".$this->value."'";
  }

}

 ?>

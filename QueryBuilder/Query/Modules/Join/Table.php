<?php
namespace QueryBuilder\Query\Modules\Join;

use QueryBuilder\Access\Renderable;

class Table implements Renderable
{

    protected $table;
    protected $dependency;

    function __construct(Join $dep)
    {
        $this->dependency = $dep;
    }

    public function render()
    {
        return ' '.$this->table;
    }

    public function set($table)
    {
        $this->table = $table;
        return $this->dependency;
    }
}
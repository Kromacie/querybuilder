<?php
namespace QueryBuilder\Query\Modules\Join;

use QueryBuilder\Query\Modules\Dependency;
use QueryBuilder\Query\Statement;

class Join extends Statement
{
    protected $type;
    protected $dependency;
    protected $tableManager;
    protected $conditionManager;

    function __construct(Dependency $dependency)
    {
        $this->dependency = $dependency;
        $this->tableManager = new Table($this);
        $this->conditionManager = new Where($this);
    }

    public function inner()
    {
        $this->type = "INNER";
        return $this;
    }

    public function outer()
    {
        $this->type = "OUTER";
        return $this;
    }

    public function left()
    {
        $this->type = "LEFT";
        return $this;
    }

    public function right()
    {
        $this->type = "RIGHT";
        return $this;
    }

    public function where()
    {
        return $this->conditionManager;
    }

    public function render()
    {
        $query = isset($this->type) ? $this->type.' ': '';
        $query .= 'JOIN';
        $query .= $this->tableManager->render();
        $query .= $this->conditionManager->render();
        return $query;
    }

    public function table()
    {
        return $this->tableManager;
    }

}
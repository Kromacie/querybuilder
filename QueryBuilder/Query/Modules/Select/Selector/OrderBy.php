<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\VariablesAccess;
use QueryBuilder\Query\Modules\Expression;
use QueryBuilder\Query\Modules\Select\Selector\OrderBy\Order;

class OrderBy implements Renderable, ColumnAccess
{
    protected $orders = [];
    protected $row;
    protected $buffer;
    protected $last;

    function __construct(Expression $expression)
    {
        $this->expression = $expression;
        $this->buffer = new Order($this);
    }

    function column($row, $table = false)
    {
        $order = clone $this->buffer;
        $this->last = $order;
        if($table)
        {
            $order->setTable($table);
        }
        $order->setRow($row);
        $this->orders[] = $order;
        return $order;
    }

    function render()
    {
        $query = "";
        foreach ($this->orders as $order)
        {
            $query .= " ".$order->render().",";
        }
        if($query != "")
        {
            return " ORDER BY".substr($query, 0, -1);
        }
        return "";
    }

}
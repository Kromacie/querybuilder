<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Query\Modules\Select\Selector;

/**
 *
 */
class Avg extends Operator
{

    function __construct(Selector $select)
    {
        $this->select = $select;
    }
    function render()
    {
        return isset($this->table) ? "AVG($this->table".".$this->row)" : "AVG($this->row)";
    }
}

?>

<?php
namespace QueryBuilder\Query\Modules\Select;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Modules\From;
use QueryBuilder\Query\Modules\Select\Selector\Avg;
use QueryBuilder\Query\Modules\Select\Selector\Count;
use QueryBuilder\Query\Modules\Select\Selector\Normal;
use QueryBuilder\Query\Modules\Select\Selector\Sum;
use QueryBuilder\Query\Select;
/**
 *
 */
class Selector implements ColumnAccess, Renderable
{
    protected $select;
    protected $operators = [];
    function __construct(Select $select)
    {
        $this->select = $select;
    }
    function column($column, $table = false)
    {
        $operator = new Normal($this);
        $operator->column($column);
        if($table){
            $operator->set($table);
        }
        $this->operators[$column] = $operator;
        return $this;
    }
    function count($column, $table = false)
    {
        $operator = new Count($this);
        $operator->column($column);
        if($table){
            $operator->set($table);
        }
        $this->operators[$column] = $operator;
        return $this;
    }
    function sum($column, $table = false)
    {
        $operator = new Sum($this);
        $operator->column($column);
        if($table){
            $operator->set($table);
        }
        $this->operators[$column] = $operator;
        return $this;
    }
    function avg($column, $table = false)
    {
        $operator = new Avg($this);
        $operator->column($column);
        if($table){
            $operator->set($table);
        }
        $this->operators[$column] = $operator;
        return $this;
    }

    function render()
    {
        $query = "";
        foreach ($this->operators as $operator) {
            $query .= " ".$operator->render().",";
        }

        return substr($query, 0, -1);
    }

}

?>

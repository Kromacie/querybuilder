<?php
namespace QueryBuilder\Query\Modules\Insert\RowSpl;

use QueryBuilder\Query\Modules\Insert\RowSpl;

class Row
{
    protected $row;
    protected $name;
    function __construct(RowSpl $row)
    {
        $this->row = $row;
    }
    function setRow($row)
    {
        $this->name = $row;
    }
    function get()
    {
        return $this->name;
    }
}
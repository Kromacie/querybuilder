<?php
namespace QueryBuilder\Query\Modules\Insert;

use QueryBuilder\Query\Insert;
use QueryBuilder\Query\Modules\Insert\RowSpl\Value;
use QueryBuilder\Query\Statement;

class ValueSpl
{
    protected $insert;
    protected $values = [];
    function __construct(Insert $insert)
    {
        $this->insert = $insert;
    }
    function value($value)
    {
        $ob = new Value($this);
        $ob->set($value);
        $this->values[] = $ob;
        return $this->insert->insert();
    }
    function render()
    {
        $query = " VALUES (";
        foreach ($this->values as $value)
        {
            /** @var Value $value */
            $query .= "'".$value->get()."',";
        }
        return substr($query, 0, -1).")";
    }
}
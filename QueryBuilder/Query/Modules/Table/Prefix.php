<?php
namespace QueryBuilder\Query\Modules\Table;

use QueryBuilder\Access\Renderable;

class Prefix implements Renderable
{
    protected $prefix;
    function setPrefix($name)
    {
        $this->prefix = $name;
    }

    public function render()
    {
        if(isset($this->prefix))
        {
            return " ".$this->prefix;
        }
        return "";
    }
}
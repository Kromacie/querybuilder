<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Access\Renderable;
use QueryBuilder\Query\Modules\Join\Join;

class Dependency implements Renderable
{
    protected $joins = [];
    protected $join;

    function __construct()
    {
        $this->join = new Join($this);
    }

    public function join()
    {
        $join = clone $this->join;
        $this->joins[] = $join;
        return $join;
    }

    public function render()
    {
        $query = ' ';
        foreach ($this->joins as $join)
        {
            $query .= $join->render();
        }
        return $query;
    }
}
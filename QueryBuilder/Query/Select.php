<?php
namespace QueryBuilder\Query;

use QueryBuilder\Query\Modules\Dependency;
use QueryBuilder\Query\Modules\Expression;
use QueryBuilder\Query\Modules\From;
use QueryBuilder\Query\Modules\Select\Selector;
use QueryBuilder\Query\Modules\Select\Where;

class Select extends Statement
{
    protected $variableManager;
    protected $conditionManager;
    protected $tableManager;
    protected $expressionManager;
    protected $dependencyManager;

    function __construct()
    {
        $this->variableManager = new Selector($this);
        $this->tableManager = new From($this);
        $this->dependencyManager = new Dependency();
        $this->conditionManager = new Where($this);
        $this->expressionManager = new Expression($this);

    }

    public function render()
    {
        $query = "SELECT";
        $query .= $this->variableManager->render();
        $query .= $this->tableManager->render();
        $query .= $this->dependencyManager->render();
        $query .= $this->conditionManager->render();
        $query .= $this->expressionManager->render();
        return $query.";";
    }

    public function dependency()
    {
        return $this->dependencyManager;
    }

    public function expression()
    {
        return $this->expressionManager;
    }

    public function where()
    {
        return $this->conditionManager;
    }

    public function table()
    {
        return $this->tableManager;
    }

    function select()
    {
        return $this->variableManager;
    }
}
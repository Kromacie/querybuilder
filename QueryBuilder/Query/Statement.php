<?php
namespace QueryBuilder\Query;

use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\ExpressionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\SelectorAccess;
use QueryBuilder\Access\TableAccess;

abstract class Statement implements Renderable, TableAccess, ConditionsAccess
{
    protected $variableManager;
    protected $conditionManager;
    protected $tableManager;
    protected $expressionManager;
}
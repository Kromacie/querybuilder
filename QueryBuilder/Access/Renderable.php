<?php
namespace QueryBuilder\Access;

interface Renderable
{
    public function render();
}
<?php
namespace QueryBuilder\Access;

interface SelectorAccess
{
    function variables();
}
